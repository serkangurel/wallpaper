package com.serkangurel.wallpaper.fragments;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.orhanobut.hawk.Hawk;
import com.serkangurel.wallpaper.R;
import com.serkangurel.wallpaper.activities.WallpaperActivity;
import com.serkangurel.wallpaper.customviews.MyImageView;
import com.serkangurel.wallpaper.databinding.FragmentMainBinding;
import com.serkangurel.wallpaper.interfaces.OnFragmentClickListener;
import com.serkangurel.wallpaper.models.FirebaseItem;
import com.serkangurel.wallpaper.otto.UpdateFavorites;
import com.serkangurel.wallpaper.utils.Constans;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.HashMap;


/**
 * Created by Sefa on 4.06.2017.
 */

public class FavoritesFragment extends BaseFragment {

    private static final String ARG_IMAGE_LIST = "image_list";

    private FragmentMainBinding fragmentMain;

    private WallpapersRVA wallpapersRVA;
    private OnFragmentClickListener mListener;

    private ArrayList<FirebaseItem.ImagesBean> images;

    public FavoritesFragment() {
    }

    public static FavoritesFragment newInstance(ArrayList<FirebaseItem.ImagesBean> images) {
        FavoritesFragment fragment = new FavoritesFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_IMAGE_LIST, images);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            images = (ArrayList<FirebaseItem.ImagesBean>) getArguments().getSerializable(ARG_IMAGE_LIST);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        fragmentMain = DataBindingUtil.inflate(inflater, R.layout.fragment_main, container, false);

        GridLayoutManager staggeredGridLayoutManager = new GridLayoutManager(getContext(), 2);
        fragmentMain.recyclerview.setLayoutManager(staggeredGridLayoutManager);

        wallpapersRVA = new WallpapersRVA(getContext(), images);
        fragmentMain.recyclerview.setAdapter(wallpapersRVA);

        if (wallpapersRVA.getItemCount() > 0) {
            fragmentMain.emptyLayout.setVisibility(View.GONE);
            fragmentMain.recyclerview.setVisibility(View.VISIBLE);
        } else {
            fragmentMain.emptyLayout.setVisibility(View.VISIBLE);
            fragmentMain.recyclerview.setVisibility(View.GONE);
        }

        return fragmentMain.getRoot();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentClickListener) {
            mListener = (OnFragmentClickListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Subscribe
    public void updateFavorites(UpdateFavorites event) {
        HashMap<String,FirebaseItem.ImagesBean> favorites = Hawk.get(Constans.HAWK_FAVORITES);
        if (favorites != null) {
            ArrayList<FirebaseItem.ImagesBean> images = new ArrayList<>();
            images.addAll(favorites.values());
            wallpapersRVA.setItems(images);

            if (wallpapersRVA.getItemCount() > 0) {
                fragmentMain.emptyLayout.setVisibility(View.GONE);
                fragmentMain.recyclerview.setVisibility(View.VISIBLE);
            } else {
                fragmentMain.emptyLayout.setVisibility(View.VISIBLE);
                fragmentMain.recyclerview.setVisibility(View.GONE);
            }

        }

    }

    public class WallpapersRVA extends RecyclerView.Adapter<WallpapersRVA.ImageHolder> {

        private ArrayList<FirebaseItem.ImagesBean> imageList = new ArrayList<>();
        private Context context;

        public WallpapersRVA(Context context, ArrayList<FirebaseItem.ImagesBean> imageList) {
            this.context = context;
            this.imageList.addAll(imageList);
        }

        public void setItems(ArrayList<FirebaseItem.ImagesBean> imageList) {
            if (imageList != null) {
                this.imageList.clear();
                this.imageList.addAll(imageList);
                notifyDataSetChanged();
            }
        }

        @Override
        public ImageHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(context).inflate(R.layout.item_main_rva, parent, false);
            return new ImageHolder(view);
        }

        @Override
        public void onBindViewHolder(final ImageHolder holder, final int position) {
            holder.image.setTag(position);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                holder.image.setTransitionName("transition_name" + position);
            }

            holder.image.setThumbUrl(imageList.get(position).url);

            holder.view.setOnClickListener(v -> {
                GridLayoutManager sglm = (GridLayoutManager) fragmentMain.recyclerview.getLayoutManager();
                int firstVisibleItem = sglm.findFirstVisibleItemPosition();
                int lastVisibleItem = sglm.findLastVisibleItemPosition();
                WallpaperActivity.start(context, imageList, holder.image, firstVisibleItem, lastVisibleItem, false);
                mListener.onFragmentClick(getClass(), fragmentMain.recyclerview, position);
            });

        }

        @Override
        public int getItemCount() {
            return imageList.size();
        }

        class ImageHolder extends RecyclerView.ViewHolder {
            private MyImageView image;
            private View view;

            public ImageHolder(View itemView) {
                super(itemView);
                view = itemView;
                image = itemView.findViewById(R.id.image_item);
            }
        }
    }
}
