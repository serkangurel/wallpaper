package com.serkangurel.wallpaper.fragments;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.orhanobut.hawk.Hawk;
import com.serkangurel.wallpaper.BR;
import com.serkangurel.wallpaper.R;
import com.serkangurel.wallpaper.activities.CategoryDetailActivity;
import com.serkangurel.wallpaper.databinding.FragmentCategoriesBinding;
import com.serkangurel.wallpaper.databinding.ItemCategoriesRvaBinding;
import com.serkangurel.wallpaper.interfaces.OnFragmentClickListener;
import com.serkangurel.wallpaper.models.FirebaseItem;
import com.serkangurel.wallpaper.utils.Constans;

import java.util.ArrayList;


/**
 * Created by Sefa on 4.06.2017.
 */

public class CategoryFragment extends BaseFragment {

    private static final String ARG_CATEGORIES = "categories";

    private FragmentCategoriesBinding fragmentCategories;

    private CategoriesRVA categoriesRVA;
    private OnFragmentClickListener mListener;

    public CategoryFragment() {
    }

    public static CategoryFragment newInstance() {
        CategoryFragment fragment = new CategoryFragment();
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        fragmentCategories = DataBindingUtil.inflate(inflater, R.layout.fragment_categories, container, false);
        GridLayoutManager staggeredGridLayoutManager = new GridLayoutManager(getContext(), 2);
        fragmentCategories.recyclerview.setLayoutManager(staggeredGridLayoutManager);
        FirebaseItem fi = Hawk.get(Constans.HAWK_DATA);
        categoriesRVA = new CategoriesRVA(fi.categories);
        fragmentCategories.recyclerview.setAdapter(categoriesRVA);

        return fragmentCategories.getRoot();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentClickListener) {
            mListener = (OnFragmentClickListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public class CategoriesRVA extends RecyclerView.Adapter<CategoriesRVA.ImageHolder> {

        private ArrayList<FirebaseItem.CategoriesBean> imageList = new ArrayList<>();

        public CategoriesRVA(ArrayList<FirebaseItem.CategoriesBean> imageList) {
            this.imageList.addAll(imageList);
        }

        @Override
        public ImageHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            ItemCategoriesRvaBinding viewDataBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_categories_rva, parent, false);
            return new ImageHolder(viewDataBinding);
        }

        @Override
        public void onBindViewHolder(final ImageHolder holder, final int position) {
            FirebaseItem.CategoriesBean item = imageList.get(position);
            ItemCategoriesRvaBinding viewDataBinding = holder.getViewDataBinding();
            viewDataBinding.setVariable(BR.data, item);

            viewDataBinding.getRoot().setOnClickListener(v -> {

                CategoryDetailActivity.start(getContext(), item.id, item.name);
                mListener.onFragmentClick(getClass(), fragmentCategories.recyclerview, position);
            });

        }

        @Override
        public int getItemCount() {
            return imageList.size();
        }


        class ImageHolder extends RecyclerView.ViewHolder {

            private ItemCategoriesRvaBinding viewDataBinding;

            public ImageHolder(ItemCategoriesRvaBinding itemView) {
                super(itemView.getRoot());
                viewDataBinding = itemView;
            }

            public ItemCategoriesRvaBinding getViewDataBinding() {
                return viewDataBinding;
            }
        }

    }
}
