package com.serkangurel.wallpaper.interfaces;

import android.support.v7.widget.RecyclerView;

/**
 * Created by Sefa on 25.06.2017.
 */

public interface OnFragmentClickListener {
    void onFragmentClick(Class<?> clazz, RecyclerView recyclerView, int position);
}
