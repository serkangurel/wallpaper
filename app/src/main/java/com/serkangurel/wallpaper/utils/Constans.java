package com.serkangurel.wallpaper.utils;

/**
 * Created by Sefa on 25.06.2017.
 */

public class Constans {

    public static String ADMOB_BANNER_KEY;

    public static String ADMOB_INTERSTITIAL_KEY;

    public static final String HAWK_DATA = "firebase_data";

    public static final String HAWK_FAVORITES = "favorites";

    public static final String INTENT_KEY = "image_url";

    public static final int MAX_NEW_IMAGES = 100;

}
