package com.serkangurel.wallpaper.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Environment;
import android.widget.ImageView;

import com.serkangurel.wallpaper.BuildConfig;
import com.serkangurel.wallpaper.GlideApp;
import com.serkangurel.wallpaper.R;

import java.io.File;

/**
 * Created by Sefa on 25.06.2017.
 */

public class Tools {

    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }

    public static int getStatusBarHeight(Context context) {
        int result = 0;
        int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = context.getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    public static String getAppName() {
        String appId = BuildConfig.APPLICATION_ID;
        String[] myArray = appId.split("\\.");
        return myArray[myArray.length - 1];
    }

    public static String getDownloadPath() {
        return Environment.DIRECTORY_PICTURES + File.separator + getAppName();
    }

    public static void loadImage(ImageView imageView, String url) {
        GlideApp.with(imageView.getContext()).load(url) //
//                .placeholder(R.drawable.placeholder_square) //
                .into(imageView);

    }

    public static void loadThumbImage(ImageView imageView, String url) {
        GlideApp.with(imageView.getContext()).load(url) //
                .override(300).placeholder(R.drawable.placeholder_square) //
                .into(imageView);
    }
}
