package com.serkangurel.wallpaper.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.serkangurel.wallpaper.R;
import com.serkangurel.wallpaper.otto.BusProvider;


/**
 * Created by Sefa on 26.06.2017.
 */

public abstract class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        BusProvider.getInstance().register(this);
    }

    @Override
    protected void onDestroy() {
        BusProvider.getInstance().unregister(this);
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

    public void startActivityWithAnim(Intent intent) {
        startActivityForResult(intent, 500);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

}
