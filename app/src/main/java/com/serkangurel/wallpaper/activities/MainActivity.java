package com.serkangurel.wallpaper.activities;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.orhanobut.hawk.Hawk;
import com.serkangurel.wallpaper.BuildConfig;
import com.serkangurel.wallpaper.R;
import com.serkangurel.wallpaper.adapters.SectionsPagerAdapter;
import com.serkangurel.wallpaper.databinding.ActivityMainBinding;
import com.serkangurel.wallpaper.fragments.CategoryFragment;
import com.serkangurel.wallpaper.fragments.GalleryFragment;
import com.serkangurel.wallpaper.interfaces.OnFragmentClickListener;
import com.serkangurel.wallpaper.models.FirebaseItem;
import com.serkangurel.wallpaper.utils.Constans;
import com.serkangurel.wallpaper.utils.Tools;

import java.util.ArrayList;


public class MainActivity extends AnimatedBaseActivity implements OnFragmentClickListener {

    private ActivityMainBinding activityMain;
    private InterstitialAd mInterstitialAd;
    private FirebaseAnalytics mFirebaseAnalytics;

    public static void start(Context context, Bundle bundle) {
        Intent intent = new Intent(context, MainActivity.class);
        intent.putExtras(bundle);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityMain = DataBindingUtil.setContentView(this, R.layout.activity_main);
        setSupportActionBar(activityMain.toolbar);

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        String imgUrl = null;
        if (getIntent().hasExtra(Constans.INTENT_KEY)) {
            imgUrl = getIntent().getStringExtra(Constans.INTENT_KEY);
        }

        SectionsPagerAdapter mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        activityMain.viewpager.setAdapter(mSectionsPagerAdapter);
        activityMain.viewpager.setOffscreenPageLimit(2);
        activityMain.tablayout.setupWithViewPager(activityMain.viewpager);

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        AdView adView = new AdView(this);
        adView.setAdSize(AdSize.SMART_BANNER);
        adView.setAdUnitId(Constans.ADMOB_BANNER_KEY);
//        adView.setAdUnitId(getString(R.string.test_banner_ad_unit_id));
        activityMain.linearLayout.addView(adView, params);

        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(Constans.ADMOB_INTERSTITIAL_KEY);
        mInterstitialAd.loadAd(new AdRequest.Builder().build());

        if (!TextUtils.isEmpty(imgUrl)) {
            openPush(imgUrl);
        }
    }

    private void openPush(String imgUrl) {
        try {

            Bundle bundle = new Bundle();
            bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "push_open");
            bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, Tools.getAppName());
            mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);

            ArrayList<FirebaseItem.ImagesBean> imgBeanList = new ArrayList<>();

            FirebaseItem firebaseItem = Hawk.get(Constans.HAWK_DATA);

            for (FirebaseItem.ImagesBean image : firebaseItem.images) {
                if (imgUrl.trim().equals(image.url)) {
                    imgBeanList.add(image);
                    WallpaperActivity.start(MainActivity.this, imgBeanList, null, 0, 1, false);
                    return;
                }
            }

            FirebaseItem.ImagesBean imgBean = new FirebaseItem.ImagesBean();
            imgBean.url = imgUrl;
            imgBeanList.add(imgBean);
            WallpaperActivity.start(MainActivity.this, imgBeanList, null, 0, 1, false);

        } catch (Exception e) {

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_info) {
            try {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + BuildConfig.APPLICATION_ID)));
            } catch (android.content.ActivityNotFoundException anfe) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID)));
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onFragmentClick(Class<?> clazz, RecyclerView recyclerView, int position) {
        if (clazz == GalleryFragment.WallpapersRVA.class) {
            setCallback(recyclerView, position);
        } else if (clazz == CategoryFragment.CategoriesRVA.class) {

        }
    }

    @Override
    public void onBackPressed() {
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        }
        super.onBackPressed();
    }
}