package com.serkangurel.wallpaper.activities;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.orhanobut.hawk.Hawk;
import com.serkangurel.wallpaper.R;
import com.serkangurel.wallpaper.models.FirebaseItem;
import com.serkangurel.wallpaper.utils.Constans;
import com.serkangurel.wallpaper.utils.Tools;

import java.util.HashMap;
import java.util.Map;

public class SplashActivity extends AppCompatActivity implements ValueEventListener {

    private boolean isActivityRunning;
    private Bundle bundle;

    private String bannerKey;
    private String interstitialKey;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow();
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }

        bundle = new Bundle();

        Intent intent = getIntent();
        if (intent != null) {

            if (intent.hasExtra(Constans.INTENT_KEY)) {
                String imgUrl = intent.getStringExtra(Constans.INTENT_KEY);
                bundle.putString(Constans.INTENT_KEY, imgUrl);
            }

            String deeplink = intent.getDataString();

            if (!TextUtils.isEmpty(deeplink)) {
                bundle.putString(Constans.INTENT_KEY, deeplink);
            }
        }

        bannerKey = Tools.getAppName() + "_banner";
        interstitialKey = Tools.getAppName() + "_interstitial";

        isActivityRunning = true;

        Constans.ADMOB_BANNER_KEY = getString(R.string.default_banner);
        Constans.ADMOB_INTERSTITIAL_KEY = getString(R.string.default_interstitial);

        FirebaseRemoteConfig mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();

        Map<String,Object> defaultConfig = new HashMap<>();
        defaultConfig.put(bannerKey, Constans.ADMOB_BANNER_KEY);
        defaultConfig.put(interstitialKey, Constans.ADMOB_INTERSTITIAL_KEY);

        mFirebaseRemoteConfig.setDefaults(defaultConfig);

        boolean isNetworkConnected = Tools.isNetworkConnected(SplashActivity.this);
        if (isNetworkConnected) {

            mFirebaseRemoteConfig.fetch().addOnCompleteListener(this, task -> {

                if (task.isSuccessful()) {
                    mFirebaseRemoteConfig.activateFetched();
                }

                Constans.ADMOB_BANNER_KEY = mFirebaseRemoteConfig.getString(bannerKey);
                Constans.ADMOB_INTERSTITIAL_KEY = mFirebaseRemoteConfig.getString(interstitialKey);
            });

            FirebaseDatabase database = FirebaseDatabase.getInstance();
            DatabaseReference myRef = database.getReference(Tools.getAppName());
            myRef.addValueEventListener(this);

        } else {
            Constans.ADMOB_BANNER_KEY = mFirebaseRemoteConfig.getString(bannerKey);
            Constans.ADMOB_INTERSTITIAL_KEY = mFirebaseRemoteConfig.getString(interstitialKey);
            FirebaseItem firebaseItem = Hawk.get(Constans.HAWK_DATA);

            if (firebaseItem != null) {
                MainActivity.start(SplashActivity.this, bundle);
                finish();
            } else {
                Toast.makeText(SplashActivity.this, R.string.no_connection, Toast.LENGTH_SHORT).show();
            }

        }

    }

    @Override
    public void onDataChange(DataSnapshot dataSnapshot) {
        FirebaseItem firebaseItem = dataSnapshot.getValue(FirebaseItem.class);
//        if (firebaseItem != null) {
//            firebaseItem.removeAllNullItems();
//        }

        if (firebaseItem != null && firebaseItem.images != null && !firebaseItem.images.isEmpty()) {
            Hawk.put(Constans.HAWK_DATA, firebaseItem);

            if (isActivityRunning) {
                MainActivity.start(SplashActivity.this, bundle);
                finish();
            }
        } else {
            Toast.makeText(SplashActivity.this, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
            finish();
        }


    }

    @Override
    public void onCancelled(DatabaseError databaseError) {

    }

    @Override
    public void onStop() {
        super.onStop();
        isActivityRunning = false;
    }
}
