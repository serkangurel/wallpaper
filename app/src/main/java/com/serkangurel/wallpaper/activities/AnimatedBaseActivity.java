package com.serkangurel.wallpaper.activities;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.SharedElementCallback;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.serkangurel.wallpaper.R;

import java.util.List;
import java.util.Map;

/**
 * Created by Sefa on 26.06.2017.
 */

public abstract class AnimatedBaseActivity extends BaseActivity {

    protected int exitPosition;
    protected int enterPosition;

    @Override
    public void onActivityReenter(int resultCode, Intent data) {
        super.onActivityReenter(resultCode, data);
        if (resultCode == Activity.RESULT_OK && data != null) {
            exitPosition = data.getIntExtra("exit_position", enterPosition);
        }
    }

    @TargetApi(21)
    protected void setCallback(final RecyclerView recyclerView, final int enterPosition2) {
        enterPosition = enterPosition2;
        setExitSharedElementCallback(new SharedElementCallback() {
            @Override
            public void onMapSharedElements(List<String> names, Map<String,View> sharedElements) {

                View visibleView = recyclerView.getLayoutManager().findViewByPosition(exitPosition);

                if (exitPosition != enterPosition2 && names.size() > 0) {
                    names.clear();
                    sharedElements.clear();
                    if (visibleView != null) {
                        View view = visibleView.findViewById(R.id.image_item);
                        names.add(view.getTransitionName());
                        sharedElements.put(view.getTransitionName(), view);
                    }

                }
                setExitSharedElementCallback((SharedElementCallback) null);
            }
        });
    }


}
