package com.serkangurel.wallpaper.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.ImageView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.lyft.android.scissors.CropView;
import com.serkangurel.wallpaper.R;
import com.serkangurel.wallpaper.utils.Constans;
import com.serkangurel.wallpaper.utils.Tools;

import java.io.File;


public class CropperActivity extends AppCompatActivity {

    private static final String ARG_CROP = "crop";
    private String path;
    private CropView cropView;
    private ImageView imgSetWallpaper;

    private InterstitialAd mInterstitialAd;

    public static void start(Activity activity, String path) {
        Intent intent = new Intent(activity, CropperActivity.class);
        intent.putExtra(ARG_CROP, path);
        activity.startActivityForResult(intent, 123);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }

//        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);

        setContentView(R.layout.activity_cropper);

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(Constans.ADMOB_INTERSTITIAL_KEY);
        mInterstitialAd.loadAd(new AdRequest.Builder().build());

        new Handler().postDelayed(() -> {
            if (mInterstitialAd != null && mInterstitialAd.isLoaded()) {
                mInterstitialAd.show();
            }
        }, 3000);

        cropView = findViewById(R.id.crop_view);
        imgSetWallpaper = findViewById(R.id.img_set_wallpaper);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            path = bundle.getString(ARG_CROP);
        }

        cropView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                cropView.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                float height = cropView.getMeasuredHeight();
                float width = cropView.getMeasuredWidth();
                cropView.setViewportRatio(width / height);

                Tools.loadImage(cropView, path);
//                cropView.setImageURI(Uri.parse(path));
            }
        });


        cropView.setOnTouchListener((view, motionEvent) -> {
            int action = motionEvent.getAction();
            switch (action) {
                case MotionEvent.ACTION_DOWN:
                    imgSetWallpaper.setEnabled(false);
                    break;
                case MotionEvent.ACTION_UP:
                    imgSetWallpaper.setEnabled(true);
                    break;
                default:
                    break;
            }
            return true;
        });

        imgSetWallpaper.setOnClickListener(v -> {
            final File croppedFile = new File(getCacheDir(), "cropped.jpg");
            cropView.extensions().crop().quality(100).format(Bitmap.CompressFormat.JPEG).into(croppedFile);
            Intent intent = new Intent();
            intent.putExtra("wallpaper_file", croppedFile.getAbsolutePath());
            setResult(RESULT_OK, intent);
            finish();
        });

    }

    @Override
    protected void onDestroy() {
        mInterstitialAd = null;
        super.onDestroy();
    }
}
