package com.serkangurel.wallpaper.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.orhanobut.hawk.Hawk;
import com.serkangurel.wallpaper.R;
import com.serkangurel.wallpaper.WPApp;
import com.serkangurel.wallpaper.fragments.GalleryFragment;
import com.serkangurel.wallpaper.interfaces.OnFragmentClickListener;
import com.serkangurel.wallpaper.models.FirebaseItem;
import com.serkangurel.wallpaper.utils.Constans;

import java.util.ArrayList;

public class CategoryDetailActivity extends AnimatedBaseActivity implements OnFragmentClickListener {

    private static final String ARG_CATEGORY_ID = "category_id";
    private static final String ARG_CATEGORY_NAME = "category_name";
    private int categoryId;
    private String categoryName;
    private InterstitialAd mInterstitialAd;


    public static void start(Context context, int id, String name) {
        Intent intent = new Intent(context, CategoryDetailActivity.class);
        intent.putExtra(ARG_CATEGORY_ID, id);
        intent.putExtra(ARG_CATEGORY_NAME, name);
        ((BaseActivity) context).startActivityWithAnim(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_detail);

        ImageView imgBack = findViewById(R.id.img_back);
        imgBack.setOnClickListener(v -> onBackPressed());

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            categoryId = extras.getInt(ARG_CATEGORY_ID);
            categoryName = extras.getString(ARG_CATEGORY_NAME);
        }

        TextView tvCategoryName = findViewById(R.id.tv_category_name);
        tvCategoryName.setText(categoryName);

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(Constans.ADMOB_INTERSTITIAL_KEY);
        mInterstitialAd.loadAd(new AdRequest.Builder().build());

        new Handler().postDelayed(() -> {
            WPApp application = (WPApp) getApplication();
            if (mInterstitialAd.isLoaded() && !application.isInterstitialShown) {
                application.isInterstitialShown = true;
                mInterstitialAd.show();
            }
        }, 1500);


        FirebaseItem fi = Hawk.get(Constans.HAWK_DATA);
        ArrayList<FirebaseItem.ImagesBean> images = new ArrayList<>();
        if (categoryId > -1) {
            for (FirebaseItem.ImagesBean image : fi.images) {
                if (image.categoryId == categoryId) {
                    images.add(image);
                }
            }
        }

        LinearLayout llContent = findViewById(R.id.ll_content);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        AdView adView = new AdView(this);
        adView.setAdSize(AdSize.SMART_BANNER);
        adView.setAdUnitId(Constans.ADMOB_BANNER_KEY);
//        adView.setAdUnitId(getString(R.string.test_banner_ad_unit_id));
        llContent.addView(adView, params);

        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.container, GalleryFragment.newInstance(images));
        fragmentTransaction.commit();

    }

    @Override
    public void onFragmentClick(Class<?> clazz, RecyclerView recyclerView, int position) {
        if (clazz == GalleryFragment.WallpapersRVA.class) {
            setCallback(recyclerView, position);
        }
    }
}
