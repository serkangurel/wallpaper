package com.serkangurel.wallpaper.activities;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.ActivityOptions;
import android.app.DownloadManager;
import android.app.WallpaperManager;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.SharedElementCallback;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.esafirm.rxdownloader.RxDownloader;
import com.github.florent37.expectanim.ExpectAnim;
import com.github.florent37.expectanim.core.Expectations;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.like.LikeButton;
import com.like.OnLikeListener;
import com.orhanobut.hawk.Hawk;
import com.serkangurel.wallpaper.R;
import com.serkangurel.wallpaper.databinding.ActivityWallpaperBinding;
import com.serkangurel.wallpaper.databinding.ItemWallpapersVpaBinding;
import com.serkangurel.wallpaper.models.FirebaseItem;
import com.serkangurel.wallpaper.otto.BusProvider;
import com.serkangurel.wallpaper.otto.UpdateFavorites;
import com.serkangurel.wallpaper.utils.Constans;
import com.serkangurel.wallpaper.utils.Tools;
import com.tbruyelle.rxpermissions2.RxPermissions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WallpaperActivity extends BaseActivity implements View.OnClickListener {

    private static final String ARG_IMAGE_LIST = "image_list";
    private static final String ARG_TAG = "tag";
    private static final String ARG_FIRST_VISIBLE_ITEM = "first_visible_item";
    private static final String ARG_LAST_VISIBLE_ITEM = "last_visible_item";
    private static final String ARG_IS_ANIMATED = "is_animated";

    private ActivityWallpaperBinding activityWallpaper;

    private ArrayList<FirebaseItem.ImagesBean> listData;
    private FirebaseItem.ImagesBean currentItem;
    private FirebaseAnalytics mFirebaseAnalytics;

    private int current;
    private int firstVisibleItem;
    private int lastVisibleItem;
    private boolean isViewsInVisibleArea = true;
    private boolean isAnimated = true;

    public static void start(Context context, ArrayList<FirebaseItem.ImagesBean> listData, View view, int firstVisibleItem, int lastVisibleItem, boolean isAnimated) {
        Intent intent = new Intent(context, WallpaperActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable(ARG_IMAGE_LIST, listData);
        if (view != null) {
            bundle.putInt(ARG_TAG, (Integer) view.getTag());
        }
        bundle.putInt(ARG_FIRST_VISIBLE_ITEM, firstVisibleItem);
        bundle.putInt(ARG_LAST_VISIBLE_ITEM, lastVisibleItem);
        bundle.putBoolean(ARG_IS_ANIMATED, isAnimated);
        intent.putExtras(bundle);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && isAnimated) {
            ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation((AppCompatActivity) context, view, view.getTransitionName());
            ((AppCompatActivity) context).startActivityForResult(intent, 0, options.toBundle());
        } else {
            ((BaseActivity) context).startActivityWithAnim(intent);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = this.getIntent();
        Bundle bundle = intent.getExtras();

        listData = (ArrayList<FirebaseItem.ImagesBean>) bundle.getSerializable(ARG_IMAGE_LIST);
        current = bundle.getInt(ARG_TAG, 0);
        firstVisibleItem = bundle.getInt(ARG_FIRST_VISIBLE_ITEM);
        lastVisibleItem = bundle.getInt(ARG_LAST_VISIBLE_ITEM);
        isAnimated = bundle.getBoolean(ARG_IS_ANIMATED);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && isAnimated) {
            postponeEnterTransition();
        }

        activityWallpaper = DataBindingUtil.setContentView(this, R.layout.activity_wallpaper);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        ColorDrawable colorDrawable = new ColorDrawable(getResources().getColor(android.R.color.black));
        activityWallpaper.container.setBackgroundDrawable(colorDrawable);

        HashMap<String, FirebaseItem.ImagesBean> savedList = Hawk.get(Constans.HAWK_FAVORITES, new HashMap<>());

        for (FirebaseItem.ImagesBean item : listData) {
            if (savedList.containsKey(item.url)) {
                item.isFavorite = true;
            }
        }

        WallpapersVPA pagerAdapter = new WallpapersVPA(this, listData);
        activityWallpaper.viewpager.setAdapter(pagerAdapter);
        activityWallpaper.viewpager.setOffscreenPageLimit(3);
        activityWallpaper.viewpager.setCurrentItem(current);

        activityWallpaper.imgBack.setOnClickListener(v -> onBackPressed());

        activityWallpaper.imgShare.setOnClickListener(this);
        activityWallpaper.setWallpaper.setOnClickListener(this);
        activityWallpaper.download.setOnClickListener(this);
        activityWallpaper.share.setOnClickListener(this);


        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        AdView adView = new AdView(this);
        adView.setAdSize(AdSize.SMART_BANNER);
        adView.setAdUnitId(Constans.ADMOB_BANNER_KEY);
//        adView.setAdUnitId(getString(R.string.test_banner_ad_unit_id));
        activityWallpaper.llAdmob.addView(adView, params);

        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);

        activityWallpaper.btnHeart.setOnLikeListener(new OnLikeListener() {
            @Override
            public void liked(LikeButton likeButton) {
                HashMap<String, FirebaseItem.ImagesBean> favorites = Hawk.get(Constans.HAWK_FAVORITES, new HashMap<>());
                favorites.put(currentItem.url, currentItem);
                Hawk.put(Constans.HAWK_FAVORITES, favorites);
                currentItem.isFavorite = true;
                BusProvider.getInstance().post(new UpdateFavorites());
            }

            @Override
            public void unLiked(LikeButton likeButton) {
                HashMap<String, FirebaseItem.ImagesBean> favorites = Hawk.get(Constans.HAWK_FAVORITES, new HashMap<>());
                favorites.remove(currentItem.url);
                Hawk.put(Constans.HAWK_FAVORITES, favorites);
                currentItem.isFavorite = false;
                BusProvider.getInstance().post(new UpdateFavorites());
            }
        });

        activityWallpaper.viewpager.addOnPageChangeListener(new ViewpagerListener(current));

        introAnimations();
    }

    @Override
    public void onClick(View view) {

        activityWallpaper.fabMenu.close(true);

        if (TextUtils.isEmpty(currentItem.name)) {
            currentItem.name = "image_" + Tools.getAppName();
        }

        switch (view.getId()) {
            case R.id.set_wallpaper:
                CropperActivity.start(WallpaperActivity.this, currentItem.url);
                break;
            case R.id.download:

                RxPermissions rxPermissions = new RxPermissions(this);
                rxPermissions.request(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE).subscribe(granted -> {
                    if (granted) {
                        Toast.makeText(WallpaperActivity.this, getString(R.string.downloading), Toast.LENGTH_SHORT).show();

                        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(currentItem.url));
                        request.setDescription(getString(R.string.downloading));
                        request.setMimeType("image/jpg");
                        request.setDestinationInExternalPublicDir(Tools.getDownloadPath(), currentItem.name + ".jpg");
                        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);

                        RxDownloader.getInstance(WallpaperActivity.this).download(request).subscribe(path -> {

                        }, throwable -> {

                        });
                    }
                });

                break;
            case R.id.share:
            case R.id.img_share:

                RxPermissions rxPermissions2 = new RxPermissions(this);
                rxPermissions2.request(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE).subscribe(granted -> {
                    if (granted) {
                        Toast.makeText(this, R.string.sharing, Toast.LENGTH_LONG).show();
                        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(currentItem.url));
                        request.setMimeType("image/jpg");
                        request.setDestinationInExternalPublicDir(Tools.getDownloadPath(), currentItem.name + ".jpg");
                        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_HIDDEN);

                        RxDownloader.getInstance(WallpaperActivity.this).download(request).subscribe(path -> {

                            Intent shareIntent = new Intent();
                            shareIntent.setAction(Intent.ACTION_SEND);
                            shareIntent.putExtra(Intent.EXTRA_TEXT, currentItem.name);
                            shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse(path));
                            shareIntent.setType("image/jpg");
                            shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                            startActivity(Intent.createChooser(shareIntent, getString(R.string.send)));

                        }, throwable -> {

                        });
                    }
                });


                break;

        }

    }

    class ViewpagerListener implements ViewPager.OnPageChangeListener {

        public ViewpagerListener(int selectedPosition) {
            onPageSelected(selectedPosition);
        }

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            currentItem = listData.get(position);
            activityWallpaper.btnHeart.setLiked(currentItem.isFavorite);
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    }

    @Override
    public void finishAfterTransition() {
        if (isAnimated) {
            int pos = activityWallpaper.viewpager.getCurrentItem();
            Intent intent = new Intent();
            intent.putExtra("exit_position", pos);
            setResult(RESULT_OK, intent);

            if (pos >= firstVisibleItem && pos <= lastVisibleItem) {
                if (current != pos) {
                    View view = activityWallpaper.viewpager.findViewWithTag(pos);
                    setSharedElementCallback(view);
                }
            }
        }
        super.finishAfterTransition();
    }

    @TargetApi(21)
    private void setSharedElementCallback(final View view) {
        setEnterSharedElementCallback(new SharedElementCallback() {
            @Override
            public void onMapSharedElements(List<String> names, Map<String, View> sharedElements) {
                names.clear();
                sharedElements.clear();
                names.add(view.getTransitionName());
                sharedElements.put(view.getTransitionName(), view);
            }
        });
    }

    @TargetApi(21)
    private void setStartPostTransition(final View sharedView) {
        sharedView.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                if (isAnimated) {
                    sharedView.getViewTreeObserver().removeOnPreDrawListener(this);
                    startPostponedEnterTransition();
                }
                return false;
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK && requestCode == 123 && data != null) {
            String path = data.getStringExtra("wallpaper_file");

            new android.os.Handler().postDelayed(() -> {
                boolean isSuccess = setWallpaper(path);

                if (isSuccess) {
                    Toast.makeText(WallpaperActivity.this, getString(R.string.set_as_background), Toast.LENGTH_SHORT).show();

                    Bundle bundle = new Bundle();
                    bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "set_wallpaper");
                    bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, Tools.getAppName());
                    mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);

                } else {
                    Toast.makeText(WallpaperActivity.this, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
                }

            }, 800);


        }

    }

    private boolean setWallpaper(String path) {
        try {
            Bitmap bitmap = BitmapFactory.decodeFile(path);

            if (bitmap == null) {
                return false;
            }

            WallpaperManager wallpaperManager = WallpaperManager.getInstance(WallpaperActivity.this);
            wallpaperManager.setBitmap(bitmap);
            return true;
        } catch (IOException ex) {
        }
        return false;
    }

    public class WallpapersVPA extends PagerAdapter {

        Context context;
        ArrayList<FirebaseItem.ImagesBean> listData;

        public WallpapersVPA(Context context, ArrayList<FirebaseItem.ImagesBean> listData) {
            this.context = context;
            this.listData = listData;
        }

        @Override
        public int getCount() {
            return listData.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            ItemWallpapersVpaBinding itemWallpapersVpa = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.item_wallpapers_vpa, container, false);

            Tools.loadImage(itemWallpapersVpa.image, listData.get(position).url);
//            itemWallpapersVpa.image.setScaleType(ImageView.ScaleType.FIT_XY);

            itemWallpapersVpa.image.setOnClickListener(v -> {
                if (isViewsInVisibleArea) {
                    viewsOutsideScreen();
//                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
//                        getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
//                    }
                } else {
                    viewsInsideScreen();
//                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
//                        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
//                    }
                }
                isViewsInVisibleArea = !isViewsInVisibleArea;
            });

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && isAnimated) {
                itemWallpapersVpa.image.setTag(position);
                itemWallpapersVpa.image.setTransitionName("transition_name" + position);
                if (position == current) {
                    setStartPostTransition(itemWallpapersVpa.image);
                }
            }

            container.addView(itemWallpapersVpa.getRoot());
            return itemWallpapersVpa.getRoot();
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

    }

    private void introAnimations() {
        new ExpectAnim().expect(activityWallpaper.toolbar).toBe(Expectations.outOfScreen(Gravity.TOP)).expect(activityWallpaper.fabMenu).toBe(Expectations.outOfScreen(Gravity.BOTTOM)).toAnimation().setNow();

        new android.os.Handler().postDelayed(() -> {
            viewsInsideScreen();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            }
        }, 400);
    }

    private void viewsInsideScreen() {
        new ExpectAnim().expect(activityWallpaper.toolbar).toBe(Expectations.atItsOriginalPosition()).expect(activityWallpaper.fabMenu).toBe(Expectations.atItsOriginalPosition()).toAnimation().setDuration(200).start();
    }

    private void viewsOutsideScreen() {

        if (activityWallpaper.fabMenu.isOpened()) {
            activityWallpaper.fabMenu.close(true);
        }

        new ExpectAnim().expect(activityWallpaper.toolbar).toBe(Expectations.outOfScreen(Gravity.TOP)).expect(activityWallpaper.fabMenu).toBe(Expectations.outOfScreen(Gravity.BOTTOM)).toAnimation().setDuration(200).start();
    }

    @Override
    public void onBackPressed() {
        activityWallpaper.toolbar.setVisibility(View.INVISIBLE);
        activityWallpaper.fabMenu.setVisibility(View.INVISIBLE);
        super.onBackPressed();
    }
}
