//package com.sgmobile.wallpaper.adapters;
//
//import android.content.Context;
//import android.os.Build;
//import android.support.v7.widget.RecyclerView;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.ImageView;
//
//import com.sgmobile.wallpaper.R;
//import com.sgmobile.wallpaper.activities.WallpaperActivity;
//import com.sgmobile.wallpaper.models.Item;
//import com.squareup.picasso.Picasso;
//
//import java.util.ArrayList;
//
///**
// * Created by Serkan on 5.6.2017.
// */
//
//public class CategoriesRVA extends RecyclerView.Adapter<CategoriesRVA.ImageHolder> {
//
//    private ArrayList<Item.ItemsBean> listData;
//    private Context context;
//
//    public CategoriesRVA(Context context, ArrayList<Item.ItemsBean> listData) {
//        this.context = context;
//        this.listData = listData;
//    }
//
//    @Override
//    public ImageHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        View view = LayoutInflater.from(context).inflate(R.layout.item_wallpapers_rva, parent, false);
//        return new ImageHolder(view);
//    }
//
//    @Override
//    public void onBindViewHolder(final ImageHolder holder, final int position) {
//        holder.image.setTag(position);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            holder.image.setTransitionName("transition_name" + position);
//        }
//        Picasso.with(holder.image.getContext()).load(listData.get(position).url).into(holder.image);
//        holder.view.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                WallpaperActivity.start(context, listData, position, holder.image);
//            }
//        });
//
//    }
//
//    @Override
//    public int getItemCount() {
//        return listData.size();
//    }
//
//    class ImageHolder extends RecyclerView.ViewHolder {
//        private ImageView image;
//        private View view;
//
//        public ImageHolder(View itemView) {
//            super(itemView);
//            view = itemView;
//            image = (ImageView) itemView.findViewById(R.id.image_item);
//        }
//    }
//}