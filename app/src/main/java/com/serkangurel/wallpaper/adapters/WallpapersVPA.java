//package com.sgmobile.wallpaper.adapters;
//
//import android.content.Context;
//import android.support.v4.view.PagerAdapter;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.LinearLayout;
//
//import com.github.chrisbanes.photoview.PhotoView;
//import com.sgmobile.wallpaper.R;
//import com.sgmobile.wallpaper.models.Item;
//import com.squareup.picasso.Picasso;
//
//import java.util.ArrayList;
//
//
///**
// * Created by Serkan on 7.6.2017.
// */
//
//public class WallpapersVPA extends PagerAdapter {
//
//    Context context;
//    ArrayList<Item.ItemsBean> listData;
//
//    public WallpapersVPA(Context context, ArrayList<Item.ItemsBean> listData) {
//        this.context = context;
//        this.listData = listData;
//    }
//
//    @Override
//    public int getCount() {
//        return listData.size();
//    }
//
//    @Override
//    public boolean isViewFromObject(View view, Object object) {
//        return view == object;
//    }
//
//    @Override
//    public Object instantiateItem(ViewGroup container, int position) {
//        View itemView = LayoutInflater.from(context).inflate(R.layout.item_wallpapers_vpa, container, false);
//        PhotoView imageView = (PhotoView) itemView.findViewById(R.id.image);
//        Picasso.with(context).load(listData.get(position).url).into(imageView);
//        container.addView(itemView);
//        return itemView;
//    }
//
//    @Override
//    public void destroyItem(ViewGroup container, int position, Object object) {
//        container.removeView((LinearLayout) object);
//    }
//
//}
