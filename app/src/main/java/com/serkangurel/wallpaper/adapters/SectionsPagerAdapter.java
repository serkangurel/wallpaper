package com.serkangurel.wallpaper.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.orhanobut.hawk.Hawk;
import com.serkangurel.wallpaper.R;
import com.serkangurel.wallpaper.WPApp;
import com.serkangurel.wallpaper.fragments.CategoryFragment;
import com.serkangurel.wallpaper.fragments.FavoritesFragment;
import com.serkangurel.wallpaper.fragments.GalleryFragment;
import com.serkangurel.wallpaper.models.FirebaseItem;
import com.serkangurel.wallpaper.utils.Constans;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

/**
 * Created by Sefa on 24.06.2017.
 */

public class SectionsPagerAdapter extends FragmentPagerAdapter {

    public SectionsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                return GalleryFragment.newInstance(generateList(0));
            case 1:
                return CategoryFragment.newInstance();
            case 2:
                return FavoritesFragment.newInstance(generateList(2));
        }
        return null;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return WPApp.getContext().getString(R.string.recent);
            case 1:
                return WPApp.getContext().getString(R.string.categories);
            case 2:
                return WPApp.getContext().getString(R.string.favorites);
        }
        return null;
    }

    private ArrayList<FirebaseItem.ImagesBean> generateList(int position) {

        ArrayList<FirebaseItem.ImagesBean> images = new ArrayList<>();

        if (position == 0) {
            FirebaseItem fi = Hawk.get(Constans.HAWK_DATA);
            for (int i = 0; i < Constans.MAX_NEW_IMAGES; i++) {
                if (i < fi.images.size()) {
                    images.add(fi.images.get(i));
                } else {
                    break;
                }
            }
            Collections.shuffle(images);

        } else if (position == 2) {
            HashMap<String,FirebaseItem.ImagesBean> favorites = Hawk.get(Constans.HAWK_FAVORITES);
            if (favorites != null && !favorites.isEmpty()) {
                images.addAll(favorites.values());
            }
        }
        return images;
    }

}