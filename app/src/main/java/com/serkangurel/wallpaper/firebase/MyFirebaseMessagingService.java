package com.serkangurel.wallpaper.firebase;

import android.content.Intent;
import android.text.TextUtils;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.serkangurel.wallpaper.R;
import com.serkangurel.wallpaper.SendNotification;
import com.serkangurel.wallpaper.utils.Constans;


/**
 * Created by Sefa on 2.07.2017.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    @Override
    public void handleIntent(Intent intent) {
        //super.handleIntent(intent);

        String messageTitle = intent.getStringExtra("gcm.notification.title");
        String messageText = intent.getStringExtra("gcm.notification.body");
        String imageUrl = intent.getStringExtra(Constans.INTENT_KEY);

        if (TextUtils.isEmpty(messageTitle)) {
            messageTitle = getString(R.string.app_name);
        }

        if (TextUtils.isEmpty(messageText)) {
            messageText = "";
        }

        new SendNotification(MyFirebaseMessagingService.this, messageTitle, messageText, imageUrl).execute();

    }


}
