package com.serkangurel.wallpaper.models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Sefa on 24.06.2017.
 */

public class FirebaseItem {

    public ArrayList<CategoriesBean> categories;
    public ArrayList<ImagesBean> images;

    public static class CategoriesBean implements Serializable {
        public int id;
        public String name;
        public String url;
    }

    public static class ImagesBean implements Serializable {
        public int categoryId;
        public String name;
        public String url;
        public boolean isFavorite;
    }
//
//    public void removeAllNullItems() {
//        categories.removeAll(Collections.singleton(null));
//        images.removeAll(Collections.singleton(null));
//    }

}
