package com.serkangurel.wallpaper.bindingAdapters;

import android.databinding.BindingAdapter;
import android.widget.ImageView;

import com.serkangurel.wallpaper.utils.Tools;

/**
 * Created by Sefa on 25.06.2017.
 */

public class ImageViewUrlAdapter {

    @BindingAdapter({"imageUrl"})
    public static void loadImage(ImageView view, String imageUrl) {
        Tools.loadThumbImage(view, imageUrl);
    }

}
