package com.serkangurel.wallpaper;

import android.content.Context;
import android.support.multidex.MultiDexApplication;

import com.bumptech.glide.request.target.ViewTarget;
import com.crashlytics.android.Crashlytics;
import com.orhanobut.hawk.Hawk;

import io.fabric.sdk.android.Fabric;

/**
 * Created by Sefa on 24.06.2017.
 */

public class WPApp extends MultiDexApplication {

    public boolean isInterstitialShown = false;

    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        context = this;

        ViewTarget.setTagId(R.id.glide_request);

        Hawk.init(context)
//                .setEncryption(Encryption)
//                .setLogInterceptor(new MyLogInterceptor())
//                .setConverter(new MyConverter())
//                .setParser(new MyParser())
//                .setStorage(new MyStorage())
                .build();


    }

    public static Context getContext() {
        return context;
    }
}
