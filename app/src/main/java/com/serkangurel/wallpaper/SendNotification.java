package com.serkangurel.wallpaper;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.text.TextUtils;

import com.bumptech.glide.Glide;
import com.serkangurel.wallpaper.activities.SplashActivity;
import com.serkangurel.wallpaper.utils.Constans;


/**
 * Created by Sefa on 15.07.2017.
 */

public class SendNotification extends AsyncTask<Void,Void,Bitmap> {

    private Context mContext;
    private String title, message, imageUrl;

    public SendNotification(Context context, String title, String message, String imageUrl) {
        super();
        this.mContext = context;
        this.title = title;
        this.message = message;
        this.imageUrl = imageUrl;
    }

    @Override
    protected Bitmap doInBackground(Void... params) {

        if (TextUtils.isEmpty(imageUrl)) {
            return null;
        }

        try {
//            URL url = new URL(imageUrl);
//            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
//            connection.setDoInput(true);
//            connection.connect();
//            InputStream in = connection.getInputStream();
//            return BitmapFactory.decodeStream(in);

            return Glide.with(mContext).asBitmap().load(imageUrl).submit().get();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Bitmap result) {
        super.onPostExecute(result);

        try {
            Intent intent = new Intent(mContext, SplashActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            intent.putExtra(Constans.INTENT_KEY, imageUrl);
            PendingIntent pendingIntent = PendingIntent.getActivity(mContext, 100, intent, PendingIntent.FLAG_ONE_SHOT); // PendingIntent.FLAG_UPDATE_CURRENT

            Bitmap large_icon = BitmapFactory.decodeResource(mContext.getResources(), R.mipmap.ic_launcher_wallpaper);

            NotificationCompat.BigPictureStyle style = null;

            if (result != null) {
                style = new NotificationCompat.BigPictureStyle().bigPicture(result);
            }

            //style.setBigContentTitle("title");
            //style.setSummaryText("description");

            int smallIcon;

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                smallIcon = R.mipmap.ic_launcher_wallpaper;
            } else {
                smallIcon = R.mipmap.ic_launcher_wallpaper;
            }

            Notification notification = new NotificationCompat.Builder(mContext) //
                    .setAutoCancel(true) //
                    .setContentTitle(title) //
                    .setLargeIcon(large_icon) //
                    .setPriority(Notification.PRIORITY_MAX) //
                    .setDefaults(Notification.DEFAULT_ALL) //
                    .setContentText(message) //

                    .setContentIntent(pendingIntent) //
                    .setSmallIcon(smallIcon) //
                    .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)) //
                    .setStyle(style) //
                    .build();


            notification.flags |= Notification.FLAG_AUTO_CANCEL;

            boolean isNotificationSoundEnabled = true;
            boolean isNotificationVibrationEnabled = true;

            if (isNotificationSoundEnabled) {
                notification.defaults |= Notification.DEFAULT_SOUND;
                notification.defaults |= Notification.FLAG_SHOW_LIGHTS;

                if (isNotificationVibrationEnabled) {
                    notification.defaults |= Notification.DEFAULT_VIBRATE;
                }
            } else {
                notification.defaults = 0;
                if (isNotificationVibrationEnabled) {
                    notification.defaults |= Notification.DEFAULT_VIBRATE;
                }
            }

            notification.ledARGB = Color.BLUE;
            notification.ledOnMS = 1000;
            notification.ledOffMS = 300;
            notification.flags |= Notification.FLAG_AUTO_CANCEL;

            NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(mContext);
            notificationManagerCompat.notify(0, notification);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}