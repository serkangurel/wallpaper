package com.serkangurel.wallpaper.customviews;

import android.content.Context;
import android.os.Build;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;

import com.serkangurel.wallpaper.utils.Tools;

/**
 * Created by Sefa on 29.06.2017.
 */

public class MyToolbar extends Toolbar {

    private Context context;

    public MyToolbar(Context context) {
        super(context);
        init(context, null, 0);
    }

    public MyToolbar(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs, 0);
    }

    public MyToolbar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs, defStyleAttr);
    }

    public void init(Context ctx, AttributeSet attrs, int defStyleAttr) {
        this.context = ctx;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            int statusbarHeight = Tools.getStatusBarHeight(context);

            setPadding(0, statusbarHeight, 0, 0);

            setMeasuredDimension(widthMeasureSpec, statusbarHeight + heightMeasureSpec);
        }

    }

    // @Override
    // protected boolean fitSystemWindows(Rect insets) {
    // RelativeLayout.LayoutParams params = ((RelativeLayout.LayoutParams) this.getLayoutParams());
    // int bottom = params.bottomMargin;
    // int left = params.leftMargin;
    // int right = params.rightMargin;
    // params.setMargins(left, insets.top, right, bottom);
    // return super.fitSystemWindows(insets);
    // }

}
