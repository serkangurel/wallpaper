package com.serkangurel.wallpaper.customviews;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;

import com.serkangurel.wallpaper.utils.Tools;

public class MyImageView extends AppCompatImageView {


    public MyImageView(Context context) {
        super(context);
    }

    public MyImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MyImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setUrl(String url) {
        Tools.loadImage(this, url);
    }

    public void setThumbUrl(String url) {
        Tools.loadThumbImage(this, url);
    }

}
