package com.serkangurel.wallpaper;

import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.module.AppGlideModule;

/**
 * Created by Sefa on 15.07.2017.
 */

@GlideModule
public final class MyAppGlideModule extends AppGlideModule {
}
